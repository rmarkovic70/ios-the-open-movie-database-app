# Screenshoots 
### iOS app allows user to search movies, tv series by title or IMDb ID. App also allows user to save favorite movies using NSUserDefaults. API: "https://www.omdbapi.com/".

![0](/uploads/6f27ec3df5c1c06ddaf63cecbe397327/0.png)
![1](/uploads/2fbc56ce0db4ea10e960102ed0f6eddb/1.png)
![2](/uploads/2f9adbb664ca66dec60182e5ee24ed5f/2.png)
![3](/uploads/9952c97d3df6705378544903540cc890/3.png)
![4](/uploads/d7e2d5991064e7b3a5209b12ad2e4de2/4.png)
![5](/uploads/f9ae85c578a78d76a364f0795cef3256/5.png)
![6](/uploads/9296fe966afaf000dd42408171d02a67/6.png)
![7](/uploads/0215f5a9477a042582717e0147da2d22/7.png)
![share](/uploads/d17ccc686e59d8f6e4b8cd5c7085bb1a/share.png)
![8](/uploads/e7f45c39621beba902394fdd49eb5dce/8.png)
