//
//  Movie.swift
//  OMDB
//
//  Created by Dev on 25.12.2022..
//

import UIKit

class FavoriteMovie {
    static let shared = FavoriteMovie()
    var movie: Movie?
    var favoriteMovies:[Movie] = []
    let userDefaults = UserDefaults.standard

    
    func saveMovieToFavorites(saveObject: Movie) {
        favoriteMovies.append(saveObject)
        saveToNSUserDefaults()
    }
    
    func removeFromFavorites(movieToRemove: Movie) -> Bool {
        var index = 0
        for movie in favoriteMovies {
            if movieToRemove.title == movie.title && movieToRemove.year == movie.year {
                favoriteMovies.remove(at: index)
                saveToNSUserDefaults()
                return true
            }
            index += 1
        }
        saveToNSUserDefaults()
        return false
    }
    
    func checkIfMovieAlredyExists(instance: Movie) -> Bool {
        for movie in FavoriteMovie.shared.favoriteMovies {
            if instance.title == movie.title && instance.year == movie.year {
                return true
            }
        }
        return false
    }
    
    func saveToNSUserDefaults() {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(favoriteMovies), forKey: "myFavoriteMovies")
    }

    func retrieveFavoritesFromMemory() {
        if let data = UserDefaults.standard.value(forKey: "myFavoriteMovies") as? Data {
            let favorites = try? PropertyListDecoder().decode(Array<Movie>.self, from: data)
            self.favoriteMovies = favorites!
        }
    }
}
