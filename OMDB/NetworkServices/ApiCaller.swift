//
//  ApiCaller.swift
//  OMDB
//
//  Created by Dev on 07.12.2022..
//

import UIKit

class ApiCaller {
    static let shared = ApiCaller()
    init(){}
    
    func searchMoviesByTitle (_ movieTitle: String, _ movieYear: String, completion: @escaping (Result<Movie, NetworkingError>) -> Void) {
        guard let url = URL(string: "http://www.omdbapi.com/?t=\(movieTitle)&y=\(movieYear)&apikey=\(Tokens.api)") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async {
                guard let _ = response as? HTTPURLResponse else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    let result = try JSONDecoder().decode(Movie.self, from: data)
                    completion(.success(result))
                    
                }
                catch {
                    completion(.failure(NetworkingError.serverError))
                }
            }
        }

        task.resume()
    }
    
    func searchMoviesByImdbId (_ movieId: String, completion: @escaping (Result<Movie, NetworkingError>) -> Void) {

        guard let url = URL(string: "http://www.omdbapi.com/?i=\(movieId)&apikey=\(Tokens.api)") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async {
                guard let _ = response as? HTTPURLResponse else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    let result = try JSONDecoder().decode(Movie.self, from: data)
                    completion(.success(result))
                    
                }
                catch {
                    completion(.failure(NetworkingError.serverError))
                }
            }
        }

        task.resume()
    }
}
