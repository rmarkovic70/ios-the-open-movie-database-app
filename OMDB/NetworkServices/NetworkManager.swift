//
//  Network Manager.swift
//  OMDB
//
//  Created by Dev on 08.12.2022..
//

import Foundation

enum NetworkingError: String, Error {
    case serverError = "Server not working."
    case movieNotFoundError = "Movie not existing. Search another one."
}
