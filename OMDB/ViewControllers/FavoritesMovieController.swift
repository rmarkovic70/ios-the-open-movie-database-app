//
//  FavoritesMovieController.swift
//  OMDB
//
//  Created by Dev on 17.12.2022..
//

import UIKit

class FavoritesMovieController: UIViewController {

    static let identifier = "favorites"
    
    @IBOutlet weak var favoriteMoviesUITable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeTableView()
        reloadTableData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        reloadTableData()
    }

    func initializeTableView() {
        favoriteMoviesUITable.allowsSelection = false
        favoriteMoviesUITable.register(UINib(nibName: "FavoriteMovieCell", bundle: nil), forCellReuseIdentifier: FavoriteMovieCell.identifier)
        self.favoriteMoviesUITable.separatorColor = UIColor(named: "Light Red")
    }
    
    func reloadTableData() {
        DispatchQueue.main.async {
            self.favoriteMoviesUITable.reloadData()
        }
    }
}
