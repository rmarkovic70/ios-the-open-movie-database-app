//
//  ViewController.swift
//  OMDB
//
//  Created by Dev on 07.12.2022..
//

import UIKit 

class HomeController: UIViewController {
    var titleTextField: String?
    var yearTextField: String?
    @IBOutlet weak var searchUIButton: UIButton!
    @IBOutlet weak var movieTitleUILabel: UITextField!
    @IBOutlet weak var movieReleaseYearUILabel: UITextField!
    @IBOutlet weak var movieIMDBiDUILabel: UITextField!
    @IBOutlet weak var detailedFeaturesUIView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        retrieveFavoriteMovies()
        setupUI()
    }
    
    @IBAction func expandFeaturesUIButtonClick(_ sender: UIButton) {
        detailedFeaturesUIView.isHidden = !detailedFeaturesUIView.isHidden
    }
    
    @IBAction func movieTitleUILabelFilled(_ sender: UITextField) {
        searchUIButton.isEnabled = true
    }
    
    @IBAction func imdbIDUiLabelFilled(_ sender: UITextField) {
        searchUIButton.isEnabled = true
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        if movieTitleUILabel.text == "" && movieIMDBiDUILabel.text == "" {
            obligatoryTitleAlert()
        }
        else {
            openQueuedMovieStoryboard()
        }
    }
    
    func setupUI() {
        searchUIButton.isEnabled = false
        detailedFeaturesUIView.isHidden = true
        setupTextFieldUIs()
    }
    
    func setupTextFieldUIs() {
        movieTitleUILabel.layer.borderWidth = 1
        movieTitleUILabel.layer.borderColor = UIColor(named: "Light Grey")?.cgColor
        movieTitleUILabel.layer.cornerRadius = 8
        
        movieReleaseYearUILabel.layer.borderWidth = 1
        movieReleaseYearUILabel.layer.borderColor = UIColor(named: "Light Grey")?.cgColor
        movieReleaseYearUILabel.layer.cornerRadius = 8
        
        movieIMDBiDUILabel.layer.borderWidth = 1
        movieIMDBiDUILabel.layer.borderColor = UIColor(named: "Light Grey")?.cgColor
        movieIMDBiDUILabel.layer.cornerRadius = 8
    }
    func openQueuedMovieStoryboard() {
        DispatchQueue.main.async {
            self.buildStoryboard()
        }
    }
    
    func retrieveFavoriteMovies() {
        FavoriteMovie.shared.retrieveFavoritesFromMemory()
    }
    
    func buildStoryboard() {
        let movieListVC = UIStoryboard.init(name: "MovieList", bundle: nil).instantiateViewController(withIdentifier: "MovieListStoryboard") as! MovieListController
        movieListVC.movieTitle = self.movieTitleUILabel.text
        movieListVC.movieYear = self.movieReleaseYearUILabel.text
        movieListVC.imdbID = self.movieIMDBiDUILabel.text
        navigationController?.pushViewController(movieListVC, animated: true)
        clearUITextFields()
    }
    
    func clearUITextFields() {
        movieTitleUILabel.text?.removeAll()
        movieReleaseYearUILabel.text?.removeAll()
        movieIMDBiDUILabel.text?.removeAll()
    }
    
    // MARK: Alerts
    func obligatoryTitleAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Warning", message: "Please enter movie title or IMDb ID.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default))
            self.present(alert, animated: true)
        }
    }
}
// pozicija treptajuceg kursora na textfieldovima jel normalno da je lijevo?

