//
//  MovieInfoViewController.swift
//  OMDB
//
//  Created by Dev on 16.12.2022..
//

import UIKit

class MovieInfoController: UIViewController {
    static let identifier = "movieInfo"
    var movieObject: Movie?
    var starButtonTappedFlag = false
    var movieExistsInFavoritesFlag: Bool = false
    
    @IBOutlet weak var movieTitleAndYearUILabel: UILabel!
    @IBOutlet weak var ratedUILabel: UILabel!
    @IBOutlet weak var releasedUILabel: UILabel!
    @IBOutlet weak var runtimeUILabel: UILabel!
    @IBOutlet weak var genreUILabel: UILabel!
    @IBOutlet weak var directorUILabel: UILabel!
    @IBOutlet weak var writerUILabel: UILabel!
    @IBOutlet weak var actorsUILabel: UILabel!
    @IBOutlet weak var plotUILabel: UILabel!
    @IBOutlet weak var languageUILabel: UILabel!
    @IBOutlet weak var countryUILabel: UILabel!
    @IBOutlet weak var imdbRatingUILabel: UILabel!
    @IBOutlet weak var imdbVotesUILabel: UILabel!
    @IBOutlet weak var imdbIdUILabel: UILabel!
    @IBOutlet weak var typeUILabel: UILabel!
    @IBOutlet weak var awardsUILabel: UILabel!
    @IBOutlet weak var ratingsUILabel: UILabel!
    @IBOutlet weak var addToFavoritesUIButton: UIButton!
    @IBOutlet weak var posterUIImage: UIImageView!
    @IBOutlet weak var posterActivityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        posterActivityIndicator.startAnimating()
        setupUI()
    }
    
    func emptyStarUIImage() {
        addToFavoritesUIButton.setImage(UIImage(systemName: "star"), for: .normal)
    }
    func fillStarUIImage() {
        addToFavoritesUIButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
    }
    func changeStarButtonState() {
        starButtonTappedFlag = !starButtonTappedFlag
    }
    
    
    
    func saveMovieToFavorites() {
        guard let movieToAdd = self.movieObject else { movieObjectVarFailedAlert()
            return }
        FavoriteMovie.shared.saveMovieToFavorites(saveObject: movieToAdd)
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        guard let movieObject = movieObject else { movieObjectVarFailedAlert()
            return
        }
        
        let activityController = UIActivityViewController(activityItems: ["https://www.imdb.com/title/\(movieObject.imdbID ?? "")/"], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
        
    }
    
    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        if movieExistsInFavoritesFlag {
            guard let toRemove = self.movieObject else { movieObjectVarFailedAlert()
                return }
            if FavoriteMovie.shared.removeFromFavorites(movieToRemove: toRemove) {
                emptyStarUIImage()
                movieRemovedFromFavoritesSuccessfullyAlert()
                movieExistsInFavoritesFlag = false
            }
            else {
                movieRemovedFromFavoritesFailedAlert()
            }
        }
        else {
            guard let movieObject = movieObject else { movieObjectVarFailedAlert()
                return
            }
            if !FavoriteMovie.shared.checkIfMovieAlredyExists(instance: movieObject) {
                saveMovieToFavorites()
                fillStarUIImage()
                movieExistsInFavoritesFlag = true
            }
            else {
                movieExistsInFavoritesAlert()
            }
        }
    }
    
    func setupUI() {
        setupFavoriteUIImage()
        
        guard let movie = movieObject else { return }
        let title = movie.title ?? "Title unavailable"
        let year = movie.year ?? "Year unavailable"
        let rated = movie.rated ?? "Rated unavailable"
        let released = movie.released ?? "Release unavailable"
        let runtime = movie.runtime ?? "Runtime unavailable"
        let genre = movie.genre ?? "Genre unavailable"
        let director = movie.director ?? "Director unavailable"
        let writer = movie.writer ?? "Writer unavailable"
        let actors = movie.actors ?? "Actors unavailable"
        let plot = movie.plot ?? "Plot unavailable"
        let language = movie.language ?? "Language unavailable"
        let country = movie.country ?? "Country unavailable"
        let imdbRating = movie.imdbRating ?? "Rating unavailable"
        let imdbVotes = movie.imdbVotes ?? "Votes unavailable"
        let imdbID = movie.imdbID ?? "IMDb ID unavailable"
        let type = movie.type ?? "Type unavailable"
        let awards = movie.awards ?? "Awards unavailable"
        let posterURL = movie.poster ?? ""
        let ratings = setupRatings()
        
        movieTitleAndYearUILabel.text = "\(title) [\(year)]"
        ratedUILabel.text = "RATED: \(rated)"
        releasedUILabel.text = "RELEASED: \(released)"
        runtimeUILabel.text = runtime
        genreUILabel.text = genre
        directorUILabel.text = "DIRECTOR: \(director)"
        writerUILabel.text = "WRITER: \(writer)"
        actorsUILabel.text = "ACTORS: \(actors)"
        plotUILabel.text = "PLOT: \(plot)"
        languageUILabel.text = "LANGUAGE: \(language)"
        countryUILabel.text = "COUNTRY: \(country)"
        imdbRatingUILabel.text = "IMDB RATING: \(imdbRating)"
        imdbVotesUILabel.text = "IMDB VOTES: \(imdbVotes)"
        imdbIdUILabel.text = "IMDB ID: \(imdbID)"
        typeUILabel.text = "TYPE: \(type)"
        awardsUILabel.text = "AWARDS: \(awards)"
        ratingsUILabel.text = "RATINGS: \(ratings)"
        loadImage(with: posterURL)
    }
    
    func setupFavoriteUIImage() {
        guard let movieObject = movieObject else { movieObjectVarFailedAlert()
            return
        }
        if FavoriteMovie.shared.checkIfMovieAlredyExists(instance: movieObject) {
            fillStarUIImage()
        }
        else {
            emptyStarUIImage()
        }
    }
    
    func setupRatings() -> String {
        guard let movie = movieObject else { return "Movie error." }
        var ratings = ""
        guard let movieRatings = movie.ratings else { movieObjectRatingsFailedAlert()
            return "Ratings error."}
        for rating in movieRatings {
            ratings += "\n- " + rating.source + ": " + rating.value
        }
        return ratings
    }
    
    func loadImage(with imageURL: String){
           if let imageURL = URL(string: imageURL) {
               DispatchQueue.global().async {
                   let data = try? Data(contentsOf: imageURL)
                   if let data = data {
                       let safeImage = UIImage(data: data)
                       DispatchQueue.main.async {[weak self] in
                           self?.posterUIImage.image = safeImage
                           self?.stopAnimatingActivityIndicator()
                       }
                   }
               }
           }
       }
    
    func stopAnimatingActivityIndicator() {
        posterActivityIndicator.stopAnimating()
    }
    
    // MARK: Alerts
    func movieExistsInFavoritesAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Warning", message: "You already marked this movie as favorite.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default))
            self.present(alert, animated: true)
        }
    }
    
    func movieObjectVarFailedAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Warning", message: "Something went wrong.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default))
            self.present(alert, animated: true)
        }
    }
    
    func movieRemovedFromFavoritesSuccessfullyAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Success", message: "Movie removed from favorites.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default))
            self.present(alert, animated: true)
        }
    }
    
    func movieRemovedFromFavoritesFailedAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Warning", message: "Error happend, we failed at removing your movie from favorites.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default))
            self.present(alert, animated: true)
        }
    }
    
    func movieObjectRatingsFailedAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Warning", message: "We could not get the ratings correctly.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default))
            self.present(alert, animated: true)
        }
    }
}
