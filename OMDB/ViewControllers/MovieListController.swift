//
//  MovieListViewController.swift
//  OMDB
//
//  Created by Dev on 13.12.2022..
//

import UIKit

class MovieListController: UIViewController {
    var movie: Movie?
    var movieTitle, movieYear, imdbID: String?
    @IBOutlet weak var searchedMoviesUITable: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeTableView()
        enableActivityIndicator()
        apiFindMovie()
    }
    
    func apiFindMovie() {
        if imdbID == "" {
            prepareUITextFieldString(userInput: movieTitle ?? "")
            apiSearchMovieByTitle(title: movieTitle ?? "", year: movieYear ?? "")
        }
        else {
            prepareUITextFieldString(userInput: imdbID ?? "")
            apiSearchMovieByImdbId(id: imdbID ?? "")
        }
    }
    
    func prepareUITextFieldString(userInput: String) {
        movieTitle = userInput.replacingOccurrences(of: " ", with: "+")
    }
    
    func apiSearchMovieByTitle(title: String, year: String) {
        ApiCaller.shared.searchMoviesByTitle(movieTitle ?? "", movieYear ?? "") { result in
            switch result {
            case .success(let response):
                self.checkResponse(receivedResponse: response)
                self.disableActivityIndicator()
            case .failure(let serverError):
                self.disableActivityIndicator()
                self.apiSearchFailureAlert(errorMessage: serverError.rawValue)
            }
        }
    }
    
    func apiSearchMovieByImdbId(id: String) {
        ApiCaller.shared.searchMoviesByImdbId(imdbID ?? "") { result in
            switch result {
            case .success(let response):
                self.checkResponse(receivedResponse: response)
                self.disableActivityIndicator()
            case .failure(let serverError):
                self.disableActivityIndicator()
                self.apiSearchFailureAlert(errorMessage: serverError.rawValue)
            }
        }
    }
    
    func checkResponse(receivedResponse: Movie) {
        if receivedResponse.response == "False" {
            unknownMovieAlert()
        }
        self.movie = receivedResponse
        self.reloadTableData()
    }
    
    func initializeTableView() {
        searchedMoviesUITable.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: MovieCell.identifier)
        self.searchedMoviesUITable.separatorColor = UIColor(named: "Light Red")
    }
    
    func reloadTableData() {
        DispatchQueue.main.async {
            self.searchedMoviesUITable.reloadData()
        }
    }
    
    func gotoHomeStoryboard() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func enableActivityIndicator() {
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = false
            self.activityIndicator.startAnimating()
        }
    }
    
    func disableActivityIndicator() {
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
        }
    }
    
    // MARK: Alerts
    func unknownMovieAlert(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Warning", message: NetworkingError.movieNotFoundError.rawValue, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default))
            self.present(alert, animated: true)
        }
        gotoHomeStoryboard()
    }
    
    func apiSearchFailureAlert(errorMessage: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Warning", message: errorMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .default))
            self.present(alert, animated: true)
        }
        gotoHomeStoryboard()
    }
}
