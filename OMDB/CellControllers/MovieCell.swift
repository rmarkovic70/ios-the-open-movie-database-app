//
//  MovieCell.swift
//  OMDB
//
//  Created by Dev on 08.12.2022..
//

import UIKit

class MovieCell: UITableViewCell {

    static let identifier: String = "movieCell"
    
    @IBOutlet weak var titleUILabel: UILabel!
    @IBOutlet weak var yearUILabel: UILabel!
    @IBOutlet weak var posterUIImage: UIImageView!
    @IBOutlet weak var posterActivityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        posterActivityIndicator.startAnimating()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupUI(movieObject: Movie) {
        enableActivityIndicator()
        titleUILabel.text = movieObject.title ?? ""
        yearUILabel.text = "[\(movieObject.year ?? "")]" 
        loadImage(with: movieObject.poster ?? "")
    }
    
    func enableActivityIndicator() {
        posterActivityIndicator.startAnimating()
    }
    
    func loadImage(with imageURL: String){
           if let imageURL = URL(string: imageURL) {
               DispatchQueue.global().async {
                   let data = try? Data(contentsOf: imageURL)
                   if let data = data {
                       let safeImage = UIImage(data: data)
                       DispatchQueue.main.async {[weak self] in
                           self?.posterUIImage.image = safeImage
                           self?.posterActivityIndicator.stopAnimating()
                       }
                   }
               }
           }
       }
}
