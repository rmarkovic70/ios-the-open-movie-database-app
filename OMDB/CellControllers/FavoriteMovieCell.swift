//
//  FavoriteMovieCell.swift
//  OMDB
//
//  Created by Dev on 25.12.2022..
// 

import UIKit

class FavoriteMovieCell: UITableViewCell {

    static let identifier = "FavoriteMovieCell"
    var movieObject: Movie?
    @IBOutlet weak var titleUILabel: UILabel!
    @IBOutlet weak var yearUILabel: UILabel!
    @IBOutlet weak var genreUILabel: UILabel!
    @IBOutlet weak var posterUIImage: UIImageView!
    @IBOutlet weak var removeFromFavoritesUIButton: UIButton!
    @IBOutlet weak var posterActivityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        posterActivityIndicator.startAnimating()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setupUI(movie: Movie) {
        posterActivityIndicator.startAnimating()
        removeFromFavoritesUIButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        self.movieObject = movie
        guard let movieObject = movieObject else { writeErrors()
            return }
        titleUILabel.text = movieObject.title
        yearUILabel.text = movieObject.year
        genreUILabel.text = movieObject.genre
        loadImage(with: movieObject.poster ?? "")
    }
    
    func writeErrors() {
        titleUILabel.text = "Movie error."
        yearUILabel.text = "Year error."
        genreUILabel.text = "Genre error."
    }
    
    func loadImage(with imageURL: String){
           if let imageURL = URL(string: imageURL) {
               DispatchQueue.global().async {
                   let data = try? Data(contentsOf: imageURL)
                   if let data = data {
                       let safeImage = UIImage(data: data)
                       DispatchQueue.main.async {[weak self] in
                           self?.posterUIImage.image = safeImage
                           self?.stopAnimatingActivityIndicator()
                       }
                   }
               }
           }
       }
    
    func stopAnimatingActivityIndicator() {
        posterActivityIndicator.stopAnimating()
    }
    
    @IBAction func removeFromFavoritesButtonTapped(_ sender: UIButton) {
        guard let movieObject = movieObject else { writeErrors()
            return }
        FavoriteMovie.shared.removeFromFavorites(movieToRemove: movieObject)
        removeFromFavoritesUIButton.setImage(UIImage(systemName: "star"), for: .normal)
    }
}
