//
//  UITableViewExtensions.swift
//  OMDB
//
//  Created by Dev on 07.01.2023..
//

import UIKit

extension MovieListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let movieCell = searchedMoviesUITable.dequeueReusableCell(withIdentifier: MovieCell.identifier, for: indexPath) as? MovieCell else {
            return UITableViewCell()
        }
        
        guard let movie = movie else { return movieCell }
        movieCell.setupUI(movieObject: movie)
        return movieCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieInfoVC = UIStoryboard.init(name: "MovieInfo", bundle: nil).instantiateViewController(withIdentifier: MovieInfoController.identifier) as! MovieInfoController
        guard let movie = movie else {return}
        movieInfoVC.movieObject = movie
        self.navigationController?.pushViewController(movieInfoVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 600
    }
}

extension FavoritesMovieController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let favoriteMovieCell = favoriteMoviesUITable.dequeueReusableCell(withIdentifier: FavoriteMovieCell.identifier, for: indexPath) as? FavoriteMovieCell else {
            return UITableViewCell() 
        }
        
        favoriteMovieCell.setupUI(movie: FavoriteMovie.shared.favoriteMovies[indexPath.row])
        return favoriteMovieCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FavoriteMovie.shared.favoriteMovies.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = UIView()
        let sectionTitleLabel = UILabel(frame: CGRect(x: 22, y: 0, width: self.view.frame.width, height: 30))
        sectionTitleLabel.textColor = UIColor(named: "Light Red")
        sectionTitleLabel.font = UIFont(name: "Impact", size: 20)
        sectionView.addSubview(sectionTitleLabel)
        sectionView.backgroundColor = UIColor(named: "Dark Grey")
        
        switch section {
        case 0:
            if FavoriteMovie.shared.favoriteMovies.count == 0 {
                sectionTitleLabel.text = "You have zero favorite movies."
            }
            else {
                sectionTitleLabel.text = "Your favorite movies: \(FavoriteMovie.shared.favoriteMovies.count)"
            }
            return sectionView
        default:
            sectionTitleLabel.text = "Section title error"
            return sectionView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return FavoriteMovie.shared.favoriteMovies.count != 30 ? 30 : 0
        default:
            return 0
        }
    }
}
